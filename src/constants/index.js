export const FIREBASE_CONFIG = {
        apiKey: "AIzaSyDsV7D9k5M3RCFw_5FXXRHsKVFGI7nXjE0",
        authDomain: "banner-terrainvestimentos.firebaseapp.com",
        databaseURL: "https://banner-terrainvestimentos.firebaseio.com",
        projectId: "banner-terrainvestimentos",
        storageBucket: "gs://banner-terrainvestimentos.appspot.com",
        messagingSenderId: "27809191766",        
};

export const BANNER_AREAS = [
        {
                "id": "0", 
                "name": "Visão Geral (Home)" 
        }, 
        {
                "id": "1", 
                "name": "Conta Corrente" },
        {
                "id": "2", 
                "name": "Bolsa" },
        {
                "id": "3", 
                "name": "Renda Fixa" },
        {
                "id": "4", 
                "name": "Tesouro Direto" },
        {
                "id": "5", 
                "name": "Fundos de Investimento" },
        {
                "id": "6", 
                "name": "Envio de Recursos" },
        {
                "id": "7", 
                "name": "Senha" },
        {
                "id": "8", 
                "name": "Informações Cadastrais" },
        {
                "id": "9", 
                "name": "Relatórios" },
        {
                "id": "10",
                "name" : "Plataformas" 
        }
]