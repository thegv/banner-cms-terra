import React, { Component } from 'react';
import firebase from 'firebase';
import './App.css';
import 'react-quill/dist/quill.snow.css';

import {     
  FIREBASE_CONFIG
} from './constants/index'

import UploadFormContainer from './containers/UploadFormContainer';
import BannerList from './containers/BannerList';
import LoginForm from './components/LoginForm';
import Loader from './components/Loader';

// Initialize Firebase
firebase.initializeApp(FIREBASE_CONFIG);

class App extends Component {
    constructor() {
        super();

        this.state ={
            user: null,
            loading: true
        };

        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    componentDidMount() {

        firebase.auth().onAuthStateChanged((user) => {
            if ( user ) {
                this.setState({ user, loading:false});
            }
        });
    }

    login() {
        firebase.auth().signInWithEmailAndPassword(this.state.username, this.state.password)
            .then((result) => {
                const user = result.user;
                this.setState({
                    user
                })
            })
    }

    logout() {
        firebase.auth().signOut()
            .then(() => {
                this.setState({
                    user: null
                })
            })
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {

        return (
            <div className="app">
                {this.state.user ?
                    <div className="auth">
                        <UploadFormContainer/>
                        <BannerList />
                    </div>
                :
                    <div className="login-form">
                        <LoginForm
                            handleInputChange={this.handleInputChange}
                            login={this.login}
                            loading={this.state.loading}/>
                    </div>
                }
            </div>
        );
    }
}

export default App;
