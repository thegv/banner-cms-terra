import React, { Component } from 'react';
import firebase from 'firebase';

import BannerArea from '../components/BannerArea';
import Loader from '../components/Loader';

class BannerList extends Component {

    constructor(props) {
        super(props);
        this.state = {            
            categories: [],
            loading: true
        }
    }

    componentDidMount(){
        const bannersRef =firebase.database().ref('/banners/');
        
        bannersRef.on('value', (snapshot) => {            
            let newState = [];    

            snapshot.forEach((child) => {                
                var ck = child.key;
                newState.push(ck)                
            });
            
            
            this.setState( (prevState, props) => ({
                categories: newState,
                loading: false
            }));
        })
    }

    renderLoading() {
        return (<Loader />)
    }

    renderComponent() {
        const categories = this.state.categories;
        return (
            <div className="bannerList">
            {categories.map(category =>
                <BannerArea key={category} area={category} />
            )}
            </div>
        )
    }
    render() {
        if ( this.state.loading ) {
            return this.renderLoading();
        } else if ( this.state.categories ) {
            return this.renderComponent();
        }
    }
}

export default BannerList;