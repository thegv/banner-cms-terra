import React, { Component } from 'react';
import firebase from 'firebase';

import { BANNER_AREAS } from '../constants/index';
import DropdownAreas from '../components/DropdownAreas';
import UploadButton from "../components/UploadButton";
import ImagePreview from "../components/ImagePreview";
import LinkTypeMenu from "../components/LinkTypeMenu";

import BannerLink from "../components/BannerLink";
import BannerCategory from "../components/BannerCategory";
import BannerRecommendation from "../components/BannerRecommendation";

export default class UploadFormContainer extends Component {
    constructor(props) {
        super(props);        

        this.state = {
            linkURL: "",
            bannerFile: "",
            bannerImgURL: "",
            linkType: "external",
            productID: "",
            productType: "",
            imagePreview: "",
            bannerArea: "",
            uploadProgress: 0,
            recommendations: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleImageUpload = this.handleImageUpload.bind(this);
        this.handleAreaChange = this.handleAreaChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);        
        this.handleEditorChange = this.handleEditorChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
                
        this.handleImageUpload(this.state.bannerFile);        
    }

    /**
     * @method handleInputChange
     * @description Update the state based on input changes
     * @param e - indicates the event info
     */
    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    
    handleEditorChange(value) {
        this.setState({
            recommendations: value
        })
    }

    /**
     * @method handleAreaChange
     * @description Update the state based on input changes
     * @param e - indicates the event info
     */
    handleAreaChange(e) {
        const t = e.target;
        const value = t.value;

        if (value === "external") {
            this.setState({
                productID: "",
                productType: ""
            })
        }

        this.setState({
            linkType: value
        })
    }

    /**
     * @method handleImageChange
     * @description Update the image state based on input file changes.
     *              Also reads the image/blob to return an image preview
     *              obs: this method isn't responsible for the image
     *                   upload to the server.
     *
     * @param e - indicates the event info
     */
    handleImageChange(e) {
        e.preventDefault();

        let file = e.target.files[0];
        let reader = new FileReader();

        reader.onloadend = () => {
            this.setState({
                imagePreview: reader.result,
                bannerFile: file
            })
        };

        reader.readAsDataURL(file);
    }

    /**
     * @method handleImageUpload
     * @description handle the file upload, and reset the state after it's done.
     * @param file - indicates the raw file itself
     */
    handleImageUpload(file) {
        if (!file) return;
        let reader = new FileReader();        
        this.firebaseStorageRef = firebase.storage().ref();
        reader.onloadend = () => {              
            let uploadTask = this.firebaseStorageRef.child('images/' + file.name).put(file);

            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {                   
                    this.setState({
                        uploadProgress: (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    })                                        
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            console.log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            console.log('Upload is running');
                            break;
                        default:
                            break;
                    }
                }, (error) => {
                switch (error.code) {
                    case 'storage/unauthorized':
                    // User doesn't have permission to access the object
                    break;
                    case 'storage/canceled':
                    // User canceled the upload
                    break;
                    case 'storage/unknown':
                    // Unknown error occurred, inspect error.serverResponse
                    break;
                    default:
                            break;
                }
                }, () => {                    
                    this.firebaseDbRef = firebase.database().ref('banners/' + this.state.bannerArea);
                    this.firebaseDbRef.push({
                        linkURL: this.state.linkURL,
                        bannerImgURL: uploadTask.snapshot.downloadURL,
                        bannerArea: this.state.bannerArea,
                        productID: this.state.productID,
                        productType: this.state.productType,
                        recommendations: this.state.recommendations
                    })

                    this.setState({
                        linkURL: "",
                        bannerFile: "",
                        bannerImgURL: "",
                        imagePreview: "",
                        bannerArea: "",
                        uploadProgress: 0,
                        recommendations: ""
                    })
                });                           
        };
        reader.readAsArrayBuffer(file);        
    }

    render() {
        let $imagePreview = null;
        $imagePreview = (this.state.imagePreview) ?
                        (
                            <ImagePreview
                                src={this.state.imagePreview} />
                        ) : '';


        let bannerType = null;
        
        switch(this.state.linkType) {
            case 'external':
                bannerType = <BannerLink                                
                                value={this.state.linkURL}                                
                                handleInputChange={this.handleInputChange} />   
                break;
            case 'internal':
                bannerType = <BannerCategory
                                productType={this.state.productType}
                                productID={this.state.productID}
                                handleInputChange={this.handleInputChange} />   
                break; 
            case 'recommendation':
                bannerType = <BannerRecommendation value={this.state.recommendations} handleEditorChange={this.handleEditorChange} /> 
                break;
            default:
                break;
        }   

        return (
            <div className='uploadForm__container'>
                <h2>Upload de Banners</h2>

                <form id="uploadForm" encType="multipart/form-data" onSubmit={(e) => this.handleSubmit(e)}>
                    {/* BANNER AREA SELECT */}
                    <div className="form-row">
                        <label htmlFor="bannerArea">Area</label>
                        <DropdownAreas areas={BANNER_AREAS} handleChange={this.handleInputChange}/>
                    </div>

                    {/* BANNER LINK */}
                    <div className="form-row radio-row">
                        <LinkTypeMenu handleAreaChange={this.handleAreaChange} />
                    </div>

                    {bannerType}
                                        

                    {/* BANNER IMG SELECT FILE */}
                    <div className="form-row">
                        <UploadButton key={"1"} handleImageChange={this.handleImageChange} />
                    </div>

                    {/* FILE UPLOAD PROGRESS BAR */}

                    {/* FILE PREVIEW */}
                    <div className="imagePreview">
                        <div className="progressBar" style={{width: this.state.uploadProgress + '%'} }></div>
                        {$imagePreview}
                    </div>

                    {/* SUBMIT BUTTON */}
                    <div className="form-row">
                        <button type="submit">Cadastrar Banner</button>
                    </div>
                </form>
            </div>
        )
    }
}








