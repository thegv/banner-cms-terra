import React, { Component } from 'react';

export default class ImagePreview extends Component {
    render() {
        return [
            <img src={this.props.src} alt="Preview do upload"/>
        ]
    }
}