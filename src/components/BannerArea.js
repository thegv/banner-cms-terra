import React, { Component } from 'react'
import firebase from 'firebase';
import { BANNER_AREAS } from '../constants/index';

export default class BannerArea extends Component {
    constructor(props){
        super(props);

        this.state = {
            items: [],
            loading: true
        }
    }

    componentDidMount() {
        const bannersRef = firebase.database().ref('/banners/' + this.props.area);

        bannersRef.on('value', (snapshot) => {            
            let newState = [];    

            snapshot.forEach((child) => {
                newState.push({
                    id: child.key,
                    item: child.val()
                });
            });
            
            this.setState( {
                items: newState
            });
        })
    }

    deleteItem(id) {
        if (!id) return;
        let fbdb = firebase.database().ref('/banners/' + this.props.area + '/' + id);

        fbdb.remove()
            .then( () => {
                console.log('remove sucessed');
            })
            .catch( (error) => {
                console.log('remove failed: ' + error.message);
            })

    }

    renderLoading() {
        return (
            <div className="bannerArea">
                <div className="loading">
                    <h4>Carregando...</h4>
                </div>
            </div>
        )
    }

    render() {
        let categoryName = '';

        BANNER_AREAS.filter((item) => {
            if (this.props.area === item.id) {
                categoryName = item.name                
            }

            return categoryName
        });

        return (
            <div className="bannerArea">
                <h3 className="bannerArea__title">{categoryName}</h3>                
                {this.state.items.map(banner =>
                    <div key={banner.id} className="bannerArea__images">
                        <img alt={banner.item.linkURL} src={banner.item.bannerImgURL} width="60" height="60" />
                        <button onClick={() => this.deleteItem(banner.id)} type="button" className="bt-excluir">Excluir</button>
                    </div>
                )}
            </div>
        )
    }
}
