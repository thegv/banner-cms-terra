import React, { Component } from 'react';
import Loader from './Loader';

class LoginForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: true
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }
    /**
     * @method handleInputChange
     * @description Update the state based on input changes
     * @param e - indicates the event info
     */
    handleInputChange(e) {
        this.props.handleInputChange(e);
    }

    handleSubmit(e) {
        this.props.login(e)
    }

    renderComponent() {
        return (
            <form id="loginForm">
                <h1>Login</h1>

                <div className="form-row">
                    <input
                        type="text"
                        name="username"
                        placeholder="seuusuario@email.com"
                        onChange={this.handleInputChange}
                    />
                </div>
                <div className="form-row">
                    <input
                        type="password"
                        name="password"
                        placeholder="senha"
                        onChange={this.handleInputChange}
                    />
                </div>

                <div className="form-row">
                    <button type="button" onClick={this.props.login}>Autenticar</button>
                </div>
            </form>
        )
    }

    renderLoading() {
        return (<Loader />)
    }

    render() {
        return this.renderComponent();
    }
}

export default LoginForm;