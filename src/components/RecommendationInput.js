import React, { Component } from 'react'

export default class RecommendationInput extends Component {

    constructor(props) {
        super(props);       
    }

    render() {
        return (
            <div className="form-row recommendation">
                <label htmlFor={this.props.name}>{this.props.title}</label>                
                <input type="text" id={this.props.name} name={this.props.name} />                
                <textarea />
            </div>
        )
    }
}
