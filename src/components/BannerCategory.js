import React, { Component } from 'react'

export default class BannerCategory extends Component {

    constructor(props) {
        super(props);

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(e) {
        this.props.handleInputChange(e)
    }

    render() {
        return (
            <div className="form-row">
                <label htmlFor="productType">Categoria do Produto</label>
                <select
                    name="productType"
                    value={this.props.productType}
                    onChange={this.handleInputChange}>
                    <option>Selecione</option>
                    <option value="RF">Renda Fixa</option>
                    <option value="FI">Fundo de Investimento</option>
                </select>

                <label htmlFor="linkURL">ID do Produto</label>
                <input
                    type="text"
                    name="productID"
                    value={this.props.productID}
                    placeholder="ex: '4'"
                    onChange={this.handleInputChange}
                />
            </div>
        )
    }
}
