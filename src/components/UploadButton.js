import React, { Component } from 'react'

class UploadButton extends Component {

    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    /**
     * @method handleChange
     * @description Update the image state based on input file changes.
     *              Also reads the image/blob to return an image preview
     *              obs: this method isn't responsible for the image
     *                   upload to the server.
     *
     * @param e - indicates the event info
     */
    handleChange(e) {
        this.props.handleImageChange(e);
    }

    render() {
        return [
            <input
                className="input__file__label"
                type="file"
                placeholder="Banner IM"
                name="bannerImg"
                id="bannerImg"
                onChange={this.handleChange}
            />,
            <label htmlFor="bannerImg">Selecione um arquivo</label>
        ]
    }
}

export default UploadButton