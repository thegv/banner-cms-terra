import React, { Component } from 'react'

export default class LinkTypeMenu extends Component {

    constructor(props) {
        super(props);

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(e) {
        this.props.handleInputChange(e)
    }

    render() {
        return (
            <div className="form-row">
                <label htmlFor="linkURL">Link do Banner</label>
                <input
                    type="text"
                    name="linkURL"
                    value={this.props.linkURL}
                    placeholder="http://aurldobanner.com"
                    onChange={this.handleInputChange}
                />
            </div>
        )
    }
}
