import React, { Component } from 'react';
import ReactQuill from 'react-quill';

export default class BannerRecommendation extends Component {

    constructor(props) {
        super(props);        
        this.handleEditorChange = this.handleEditorChange.bind(this)
    }

    handleEditorChange(value) {
        this.props.handleEditorChange(value)
    }

    render() {
        let styles = {
            width: '80%',
            height: '220px'
        }
        return (
            <div className="form-row">                
                <label htmlFor="recommendations">Recomendações</label>                                        
                <ReactQuill 
                    theme="snow" 
                    value={this.props.value} 
                    onChange={this.handleEditorChange}
                    style={styles} />                              
            </div>
        )
    }
}
