import React, { Component } from 'react'

export default class LinkTypeMenu extends Component {

    constructor(props) {
        super(props);

        this.handleAreaChange = this.handleAreaChange.bind(this);
    }

    handleAreaChange(e) {
        this.props.handleAreaChange(e)
    }

    render() {
        return (
            <div>
                <input type="radio" name="linkType"
                        onChange={this.handleAreaChange}
                        value="external"
                        defaultChecked="external"
                        id="linkExternal"
                    />
                <label htmlFor="linkExternal">Link Externo</label>

                <input type="radio" name="linkType"
                    onChange={this.handleAreaChange}
                    value="internal"
                    id="linkInternal"
                />
                <label htmlFor="linkInternal">Produto</label>

                <input type="radio" name="linkType"
                    onChange={this.handleAreaChange}
                    value="recommendation"
                    id="linkRecommendation"
                />
                <label htmlFor="linkRecommendation">Carteira</label>
            </div>
        )
    }
}
