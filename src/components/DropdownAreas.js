import React, { Component } from 'react'

export default class DropDownAreas extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.handleChange(e)
    }

    render() {
        return (
            <select
                name="bannerArea"
                value={this.props.value}
                onChange={this.handleChange}
                >
                    <option value="">Selecione</option>
                    {this.props.areas.map( (area) =>
                        <option key={area.id} value={area.id}>{area.name}</option>
                    )}                
            </select>            
        )
    }
}
